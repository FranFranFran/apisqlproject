package iesPauCasesnoves.cat.proves;

import iesPauCasesnoves.cat.SQL.BaseDatos;
import iesPauCasesnoves.cat.enumeracions.EnumInteriorExterior;
import iesPauCasesnoves.cat.enumeracions.EnumMaterial;
import iesPauCasesnoves.cat.enumeracions.EnumPlagues;
import iesPauCasesnoves.cat.enumeracions.EnumProductes;
import iesPauCasesnoves.cat.excepcions.SqlDBException;
import iesPauCasesnoves.cat.excepcions.StockException;
import iesPauCasesnoves.cat.menu.MenuProductes;
import iesPauCasesnoves.cat.menu.Ornamentacio;
import iesPauCasesnoves.cat.ordenacio.OrdreNom;
import iesPauCasesnoves.cat.ordenacio.OrdrePreu;
import iesPauCasesnoves.cat.tronc.Cossiol;
import iesPauCasesnoves.cat.tronc.Magatzem;
import iesPauCasesnoves.cat.tronc.Pesticida;
import iesPauCasesnoves.cat.tronc.Planta;
import iesPauCasesnoves.cat.tronc.Producte;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Enumeration;

import cat.iespaucasesnoves.swpro.xml.excepcions.GeneradorFitxersXMLException;
import cat.iespaucasesnoves.swpro.xml.generador.GeneradorFitxersXML;

public class proves {

	static ArrayList<Producte> llistaProductes = new ArrayList<Producte>();


	public static Producte getProducte(String tipus) {
		System.out.println("getproducte");
		for (Producte producte : llistaProductes) {
			if (producte.getTipus().equals(tipus)) {
				
				return producte;
			}
		}
		return null;
	}

	
	public static void cercaPartNom(String nomCom)  {
	for (int i = 0; i < llistaProductes.size(); i++) {
			if (llistaProductes.get(i).getNomCom().contains(nomCom)){
				ArrayList<Producte> cercaPartNom = new ArrayList<Producte>(); 
				cercaPartNom.add(llistaProductes.get(i));
				
				for(Producte s : cercaPartNom) //use for-each loop
				{
				    System.out.println("CERCA PART NOM" + s);
				}
				}
			
		}
	}
	
	
	/**
	 * MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN
	 * @param args
	 * @throws GeneradorFitxersXMLException
	 * @throws IOException
	 */

	public static void main(String[] args) throws GeneradorFitxersXMLException,
			IOException {
		
		// INSTANCIACIONS
		Magatzem mag1 = new Magatzem("C/ Lluna", 971111212, "AZ-00001",
				llistaProductes);
		
		Cossiol cossiol1 = new Cossiol(EnumProductes.COSSIOL, "Cossiol1", "gros", 10.0, 1.2, "A10001", 10, EnumMaterial.PLASTIC, 10.50);
		Cossiol cossiol2 = new Cossiol(EnumProductes.COSSIOL, "Cossiol2", "petit", 10.0, 1.2, "A10001", 10, EnumMaterial.PORCELANA, 5.50);
		
		mag1.afegirProducte(cossiol1);
		mag1.afegirProducte(cossiol2);

		Planta planta1 = new Planta(EnumProductes.PLANTA, "Rosa", "T� pues", 5.0, 1.2, "A-456", 50, "Resae Rosus", EnumInteriorExterior.EXTERIOR, 8.2, 10.2);
		Planta planta2 = new Planta(EnumProductes.PLANTA, "Margalida", "Es verda", 25.0, 1.2, "B-780", 35, "Margalidae", EnumInteriorExterior.INTERIOR, 12.2, 40.7);	
		Planta planta7 = new Planta(EnumProductes.PLANTA, "Flor de patata", "Es verda", 25.0, 1.2, "B-78", 85, "patatum", EnumInteriorExterior.EXTERIOR, 12.2, 40.7);
		Planta planta4 = new Planta(EnumProductes.PLANTA, "Jazm�n", "Es verda", 25.0, 1.2, "B-70", 45, "nosexd", EnumInteriorExterior.INTERIOR, 12.2, 40.7);
		Planta planta5 = new Planta(EnumProductes.PLANTA, "La flor de tu abuela", "Es verda", 25.0, 1.2, "B-780", 35, "viejussportacus", EnumInteriorExterior.EXTERIOR, 12.2, 40.7);
		Planta planta6 = new Planta(EnumProductes.PLANTA, "flora", "Es verda", 25.0, 1.2, "B-90", 3, "margarina", EnumInteriorExterior.INTERIOR, 12.2, 40.7);
		
		mag1.afegirProducte(planta1);
		mag1.afegirProducte(planta2);
		mag1.afegirProducte(planta7);
		mag1.afegirProducte(planta4);
		mag1.afegirProducte(planta5);
		mag1.afegirProducte(planta6);
		
		Pesticida pesticida1 = new Pesticida(EnumProductes.PESTICIDA, "Rai", "Olor a llimona", 12.99, 0.0, "A-456", 35, EnumPlagues.FONGS, "Imflamable", "Conservar tancat");
		Pesticida pesticida2 = new Pesticida(EnumProductes.PESTICIDA, "Zuu", "Olor a Menta", 12.99, 0.0, "A-416", 35, EnumPlagues.FORMIGUES, "Imflamable", "Conservar tancat");
		Pesticida pesticida3 = new Pesticida(EnumProductes.PESTICIDA, "Rex", "Olor a Taronja", 12.99, 0.0, "A-458", 35, EnumPlagues.LLIMACS, "Imflamable", "Conservar tancat");
		mag1.afegirProducte(pesticida1);
		mag1.afegirProducte(pesticida2);
		mag1.afegirProducte(pesticida3);

		
		/****************************
		 *          MEN�S 
		 * 	************************/
		
		MenuProductes menu = new MenuProductes();
		Ornamentacio facada = new Ornamentacio();
		
		try {
			facada.capcaleraTenda();
			facada.menuCossiols();
			facada.menuPesticides();
			facada.menuPlantes();
			facada.menuAdministracio();
			menu.menu(cossiol1, planta1, pesticida1, llistaProductes);
		} catch (StockException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
				
		}
		
		/****************************
		 * FI MEN�S
		 ***************************/
		
        System.out.println();
        System.out.println("*******************************");
        System.out.println("**   PROVES I SIMULACIONS    **");
        System.out.println("*******************************");
        System.out.println();
        
		//CERCA PER NOM
		String nomCom = "Floswdd patata";
		cercaPartNom(nomCom);
	
		
		//PROVA D'INSERCI�
		BaseDatos datos = new BaseDatos("j", nomCom, nomCom, nomCom);
		Planta poa = new Planta(EnumProductes.PLANTA, nomCom, "sdf", 8.2, 2.1, "hjk", 2, "uy",
				EnumInteriorExterior.EXTERIOR, 2.1, 5.1);
		try {
			datos.insert(poa);
		} catch (SqlDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		//NOU MAGATZEM i GENERAM XML
		Magatzem mag2 = new Magatzem("C/ Lluna", 971111212, "AZ-00001",
				llistaProductes);
		System.out.println(mag1.getXML());
		System.out.println(mag2.getXML());
		
		//GeneradorFitxersXML xml = new GeneradorFitxersXML();
		//xml.generaFitxer(mag1, "C:/Users/Miquel/Desktop/patata.xml");

		Producte planta3 =new Planta(EnumProductes.PLANTA, "Moraduixera", "Es verda", 25.0, 1.2, "B-780", 35, "Margalidae", EnumInteriorExterior.INTERIOR, 12.2, 40.7);	
		mag1.afegirProducte(planta3);
		
		//GET PRODUCTE pel tipus PLANTA
		System.out.println(mag1.getProducte(EnumProductes.PLANTA));
		
		Producte prod = new Producte(EnumProductes.PLANTA, "Blabla", "comer", 4, 3, "Cossiol", 5);
		Planta plan1 = new Planta (EnumProductes.PLANTA, "shdhd", "shdh", 0, 0, "ahaha", 0, nomCom, EnumInteriorExterior.INTERIOR, 0, 0);
		Producte prod2 = new Producte(EnumProductes.COSSIOL, "Blabla", "comer", 4, 3, "Cossiol", 5);
		
		//ORDENACI� PER PREU I NOM
		Collections.sort(llistaProductes,new OrdrePreu());
		System.out.println("Per preu" + llistaProductes + "\n");
		
		//Per nom, fa difer�ncia entre maj�scules! Si un nom �s en min�scules ho deixa al final de tot!
		Collections.sort(llistaProductes,new OrdreNom());
		System.out.println("Per nom" + llistaProductes + "\n");
		
		//CERCADOR INTERIOR-EXTERIOR
		System.out.println("Per exterior " + mag1.cercaIntExtEnum(EnumInteriorExterior.EXTERIOR));
		
		//CERCADOR PAGUES
		System.out.println("Per plaga de formigues " + mag1.cercarPesticida(EnumPlagues.FORMIGUES));
	}
	
}
