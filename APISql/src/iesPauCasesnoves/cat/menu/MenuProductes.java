package iesPauCasesnoves.cat.menu;

import iesPauCasesnoves.cat.enumeracions.EnumInteriorExterior;
import iesPauCasesnoves.cat.enumeracions.EnumMaterial;
import iesPauCasesnoves.cat.enumeracions.EnumPlagues;
import iesPauCasesnoves.cat.enumeracions.EnumProductes;
import iesPauCasesnoves.cat.excepcions.StockException;
import iesPauCasesnoves.cat.tronc.Cossiol;
import iesPauCasesnoves.cat.tronc.Magatzem;
import iesPauCasesnoves.cat.tronc.Pesticida;
import iesPauCasesnoves.cat.tronc.Planta;
import iesPauCasesnoves.cat.tronc.Producte;
import iesPauCasesnoves.cat.utilitats.UtilitatsConsola;

import java.io.IOException;
import java.util.ArrayList;

import cat.iespaucasesnoves.swpro.xml.excepcions.GeneradorFitxersXMLException;
import cat.iespaucasesnoves.swpro.xml.generador.GeneradorFitxersXML;

public class MenuProductes {

	/**
	 * Menu
	 * 
	 * @param cossiol
	 * @param planta
	 * @param llistaProductes
	 * @throws IOException
	 * @throws StockException
	 * @throws GeneradorFitxersXMLException
	 */
	public void menu(Cossiol cossiol, Planta planta, Pesticida pesticida,
			ArrayList<Producte> llistaProductes) throws IOException,
			StockException, GeneradorFitxersXMLException {

		int numero;
		UtilitatsConsola teclat = new UtilitatsConsola();
		numero = teclat.introduirPerTeclatNumero("Introdueix l'opci� del men�: ");

		Magatzem mag = new Magatzem("C/ Lluna", 971111212, "AZ-00001",
				llistaProductes);

		switch (numero) {
		case 1:
			Ornamentacio text1 = new Ornamentacio();
			text1.afegir();
			// Lectura per teclat. NO S� com llegir un String per ficar-ho a
			// EnumMaterial!!!!!!!!!!
			EnumProductes tipus = EnumProductes.COSSIOL;
			String nomCom = teclat.llegirCadena("Nom comercial: ");
			String descripcio = teclat.llegirCadena("Descripci�: ");
			double preuUnitat = teclat.llegirDouble("Preu Unitat: ");
			double descompte = teclat.llegirDouble("Descompte: ");
			String referencia = teclat.llegirCadena("Refer�ncia: ");
			int stock = teclat.introduirPerTeclatNumero("Stock: ");
			EnumMaterial material = EnumMaterial.PLASTIC; //el meu dubte!!!
			double capacitat = teclat.llegirDouble("Capacitat en llitres: ");
			Cossiol cossiolAfegir = new Cossiol(tipus, nomCom, descripcio,
					preuUnitat, descompte, referencia, stock, material,
					capacitat);
			mag.afegirProducte(cossiolAfegir);
			cossiol.incrementStock(1);
			break;

		case 2:
			Ornamentacio text2 = new Ornamentacio();
			text2.resultatCerca();
			System.out.println(mag.cercarMaterial(EnumMaterial.PORCELANA));
			break;
		case 3:
			Ornamentacio text3 = new Ornamentacio();
			text3.resultatCerca();
			System.out.println(mag.cercarMaterial(EnumMaterial.PLASTIC));
			break;
		case 4:
			Ornamentacio text4 = new Ornamentacio();
			text4.cataleg();
			System.out.println(mag.getProducte(EnumProductes.COSSIOL));
			break;
		case 5:
			/* Aqu� hauria de ser com al case 1, llegir des de teclat les dades pel constructor */
			mag.afegirProducte(planta);
			planta.incrementStock(1);
			break;

		case 6:
			Ornamentacio text6 = new Ornamentacio();
			text6.resultatCerca();
			System.out.println(mag
					.cercaIntExtEnum(EnumInteriorExterior.EXTERIOR));
			break;
		case 7:
			Ornamentacio text7 = new Ornamentacio();
			text7.resultatCerca();
			System.out.println(mag
					.cercaIntExtEnum(EnumInteriorExterior.INTERIOR));
			break;
		case 8:
			Ornamentacio text8 = new Ornamentacio();
			text8.cataleg();
			System.out.println(mag.getProducte(EnumProductes.PLANTA));
			break;

		case 9:
			/* Aqu� hauria de ser com al case 1, llegir des de teclat les dades pel constructor */
			mag.afegirProducte(pesticida);
			break;
		case 10:
			/* Aqu� hauria de ser com al case 1, llegir des de teclat les dades pel constructor */
			mag.afegirProducte(pesticida);
			pesticida.incrementStock(1);
			break;

		case 11:
			Ornamentacio text11 = new Ornamentacio();
			text11.resultatCerca();
			System.out.println(mag.cercarPesticida(EnumPlagues.FORMIGUES));
			break;
		case 12:
			Ornamentacio text12 = new Ornamentacio();
			text12.resultatCerca();
			System.out.println(mag.cercarPesticida(EnumPlagues.LLIMACS));

			break;

		case 13:
			Ornamentacio text13 = new Ornamentacio();
			text13.cataleg();
			System.out.println(mag.getProducte(EnumProductes.PESTICIDA));
			break;

		case 14:
			/* Aqu� hauria de ser com al case 1, llegir des de teclat les dades pel constructor */
			Ornamentacio text14 = new Ornamentacio();
			text14.resultatCerca();
			mag.cercaPartNom("Ro");
			break;

		case 15:
			System.out.println("GENERACI� FITXER XML");
			mag.getXML();
			break;

		case 16:
			System.out.println("Sortida de l'aplicaci�. BYEEE!!");
			return;

		}

	}
}
