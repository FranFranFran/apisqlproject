package iesPauCasesnoves.cat.menu;

public class Ornamentacio {
	
	public void capcaleraTenda(){

        System.out.println("\t _______________                                  ");
        System.out.println("\t|  ___________  |        PR�CTICA PROGRAMACI�     ");
        System.out.println("\t| |           | |     ___________________________ ");
        System.out.println("\t| |   0   0   | |    |                           |");
        System.out.println("\t| |     -   ��| |    |   Benvinguts i            |");
        System.out.println("\t| |    ___    | |   /          benvingudes       |");
        System.out.println("\t| |   [___]   | |   \\               a            |");
        System.out.println("\t| |___     ___| |    |          VERD I M�S       |");
        System.out.println("\t|_____|\\_/|_____|    |______________________ ____|");
        System.out.println("\t  _|__|/ \\|_|_                                   ");
        System.out.println("\t  / ********** \\           Miquel C�naves        ");
        System.out.println("\t /  *********** \\            Jaume Mayol         ");
        System.out.println("\t/  ***********   \\           Fran�ois Aov        ");
        System.out.println("\t----------------                                  ");
        System.out.println();
    }
    
    
    public void menuPlantes(){
        
        System.out.println();
        System.out.println("*******************************");
        System.out.println("**          COSSIOLS         **");
        System.out.println("*******************************");
        System.out.println();
        System.out.println("2.- Cercar Cossiol porcelana");
        System.out.println("3.- Cercar Cossiol pl�stic");
        System.out.println("4.- Mostrar Cat�leg de Cossiols");
		System.out.println("14. Cercar article per nom");
        System.out.println("16.- Sortir");
        System.out.println("\nSeleciona una opci� del men� i prem <Entrar>");
    }
    
    public void menuCossiols(){
        
        System.out.println();
        System.out.println("*******************************");
        System.out.println("**         PLANTES           **");
        System.out.println("*******************************");
        
        System.out.println();
        System.out.println("6. Cercar Planta exterior");
		System.out.println("7. Cercar Planta interior");
		System.out.println("8. Mostrar Cat�leg de plantes");
		System.out.println("14. Cercar article per nom");
        System.out.println("16.- Sortir");
        System.out.println("\nSeleciona una opci� del men� i prem <Entrar>");
    }
    
    public void menuPesticides(){
        
        System.out.println();
        System.out.println("*******************************");
        System.out.println("**         PESTICIDES        **");
        System.out.println("*******************************");
        System.out.println();
		System.out.println("10. Cercar Pesticida per fongs");
		System.out.println("11. Cercar Pesticida per formigues");
		System.out.println("12. Cercar Pesticida per llimags");
		System.out.println("13. Mostrar Cat�leg de Pesticides");
		System.out.println("14. Cercar article per nom");
        System.out.println("\nSeleciona una opci� del men� i prem <Entrar>");
    }
    
    public void menuAdministracio(){
        
        System.out.println();
        System.out.println("*******************************");
        System.out.println("**      ADMINISTRACI�        **");
        System.out.println("*******************************");
        System.out.println();
		System.out.println("1. Insertar Cossiol");
		System.out.println("5. Insertar Planta");
		System.out.println("9. Insertar Pesticida");
		System.out.println("14. Cercar article per nom");
		System.out.println("15. Generer XML de Magatzem");
		System.out.println("16. Sortir");
    }
    
    public void afegir(){
            System.out.println();
            System.out.println("*******************************");
            System.out.println("*****   AFEGIR PRODUCTE   *****");
            System.out.println("*******************************");
            System.out.println();
    }
    public void resultatCerca(){
        System.out.println();
        System.out.println("*******************************");
        System.out.println("**   RESULTATS DE LA CERCA   **");
        System.out.println("*******************************");
        System.out.println();
    }
    
    public void cataleg(){
        System.out.println();
        System.out.println("*******************************");
        System.out.println("**********   CAT�LEG  *********");
        System.out.println("*******************************");
        System.out.println();
    }
   
}
