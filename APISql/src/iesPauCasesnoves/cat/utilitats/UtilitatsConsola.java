package iesPauCasesnoves.cat.utilitats;

import iesPauCasesnoves.cat.enumeracions.EnumProductes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;


public class UtilitatsConsola {
	

    public String llegirCadena(String text) {
        System.out.println(text);
        Scanner lector = new Scanner(System.in);
        String cadenaLlegida = lector.next();
        lector.nextLine();
        return cadenaLlegida;
    }
	
	public int llegirSencer(String prompt) throws IOException{
		String CurLine = "";
		int numero;

		// Introduir numero per teclat
		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);

		System.out.print(prompt);
		// Numero 1
		CurLine = in.readLine();
		numero = Integer.parseInt(CurLine);
		
		return numero;
	}
	
	public double llegirDouble(String missatge) throws IOException{
		String CurLine = "";
		//int numero;

		// Introduir numero per teclat
		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);

		System.out.print(missatge);
		// Numero 1
		CurLine = in.readLine();
		int numero = Integer.parseInt(CurLine);
		return numero;
	}
	
	public int introduirPerTeclatNumero(String missatge) throws IOException {
		String CurLine = "";
		int numero;

		// Introduir numero per teclat
		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);

		System.out.print(missatge);
		// Numero 1
		CurLine = in.readLine();
		numero = Integer.parseInt(CurLine);

		return numero;

	}

	
}


