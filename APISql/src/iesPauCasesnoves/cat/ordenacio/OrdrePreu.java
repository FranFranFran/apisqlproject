package iesPauCasesnoves.cat.ordenacio;

import iesPauCasesnoves.cat.tronc.Producte;

import java.util.Comparator;

public class OrdrePreu implements Comparator<Producte>{
	
	//M�tode d'ordenaci� per preu
	
		@Override
		public int compare(Producte arg0, Producte arg1) {
			int returnVal = 0;

		    if(arg0.getPreuUnitat() < arg1.getPreuUnitat()){
		        returnVal =  -1;
		    }else if(arg0.getPreuUnitat() > arg1.getPreuUnitat()){
		        returnVal =  1;
		    }else if(arg0.getPreuUnitat() == arg1.getPreuUnitat()){
		        returnVal =  0;
		    }
		    return returnVal;
		}
		
}
