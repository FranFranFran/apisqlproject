package iesPauCasesnoves.cat.ordenacio;

import iesPauCasesnoves.cat.tronc.Producte;

import java.util.Comparator;

public class OrdreNom implements Comparator<Producte> {

	@Override
	public int compare(Producte o1, Producte o2) {
		return o1.getNomCom().compareTo(o2.getNomCom());
	}

	
}
