package iesPauCasesnoves.cat.excepcions;

public class StockException extends Exception{
	
	 private int stockCode;

    public StockException(int stockCode){
        super();
        this.stockCode = stockCode;
    }
	
	public StockException(String message){
		super(message);
		this.stockCode = stockCode;
	}

   
    public int getStockCode() {
        return this.stockCode;
    }
}
