package iesPauCasesnoves.cat.tronc;

import iesPauCasesnoves.cat.SQL.OperacionsSQL;
import iesPauCasesnoves.cat.enumeracions.EnumProductes;
import iesPauCasesnoves.cat.excepcions.StockException;

import java.util.Comparator;

public class Producte implements OperacionsSQL {
	private EnumProductes tipus;
	private String nomCom;
	private String descripcio;
	private double preuUnitat;
	private double descompte;
	private String referencia;
	protected int stock;

	public EnumProductes getTipus() {
		return tipus;
	}

	public void setTipus(EnumProductes tipus) {
		this.tipus = tipus;
	}

	public String getNomCom() {
		return nomCom;
	}

	public void setNomCom(String nomCom) {
		this.nomCom = nomCom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getPreuUnitat() {
		return preuUnitat;
	}

	public void setPreuUnitat(double preuUnitat) {
		this.preuUnitat = preuUnitat;
	}

	public double getDescompte() {
		return descompte;
	}

	public void setDescompte(double descompte) {
		this.descompte = descompte;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Producte(EnumProductes tipus, String nomCom, String descripcio,
			double preuUnitat, double descompte, String referencia, int stock) {
		super();
		this.tipus = tipus;
		this.nomCom = nomCom;
		this.descripcio = descripcio;
		this.preuUnitat = preuUnitat;
		this.descompte = descompte;
		this.referencia = referencia;
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "\n" + "Producte [tipus=" + tipus + ", nomCom=" + nomCom
				+ ", descripcio=" + descripcio + ", preuUnitat=" + preuUnitat
				+ ", descompte=" + descompte + ", referencia=" + referencia
				+ ", stock=" + stock + "] + ";
	}

	/**
	 * Aquest m�tode implementat desde l' API "OperacionsSQL" ens permet veure
	 * en un String en format DML l'objecte de tipus Producte afegit.
	 */
	public String insert() {
		String dml = "";
		dml = "INSERT INTO" + "VALUES " + getTipus() + getNomCom()
				+ getDescompte() + getPreuUnitat() + getDescompte()
				+ getReferencia() + getStock();
		return dml;
	}

	/**
	 * Aquest m�tode implementat desde l' API "OperacionsSQL" ens permet veure
	 * en un String en format DML l'objecte de tipus Producte esborrat.
	 */
	public String delete() {
		String dml = "DELETE" + "FROM " + getTipus() + getNomCom()
				+ getDescompte() + getPreuUnitat() + getDescompte()
				+ getReferencia() + getStock();
		return dml;
	}

	/**
	 * Aquest m�tode implementat desde l' API "OperacionsSQL" ens permet veure
	 * en un String en format DML l'objecte de tipus Producte modificat.
	 */
	public String update() {
		String dml = "UPDATE" + "SET" + getTipus() + getNomCom()
				+ getDescompte() + getPreuUnitat() + getDescompte()
				+ getReferencia() + getStock();
		return dml;
	}

	/**
	 * Aquest m�tode ens permet comprovar l'increment d'estoc d'un producte
	 * @param quantitat ha de ser un nombre sencer
	 * @throws StockException 
	 */
	public void incrementStock(int quantitat) throws StockException {
		if (stock < 0) {
			throw new StockException(quantitat);
		} else {
			stock = stock + quantitat;
		}
	}

	/**
	 * Aquest m�tode ens permet comprovar el decrement d'estoc d'un producte
	 * @param quantitat ha de ser un nombre sencer 
	 * @throws StockException
	 */
	public void decrementStock(int quantitat) throws StockException {
		if (stock <= 0) {
			throw new StockException(descripcio);
		} else {
			stock = stock - quantitat;
		}
	}

}
