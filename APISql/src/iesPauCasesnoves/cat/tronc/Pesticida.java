package iesPauCasesnoves.cat.tronc;

import iesPauCasesnoves.cat.enumeracions.EnumPlagues;
import iesPauCasesnoves.cat.enumeracions.EnumProductes;

public class Pesticida extends Producte {
	private EnumPlagues llistaEspecies;
	private String utilitzacio;
	private String conservacio;
	
	public EnumPlagues getLlistaEspecies() {
		return llistaEspecies;
	}
	public void setLlistaEspecies(EnumPlagues llistaEspecies) {
		this.llistaEspecies = llistaEspecies;
	}
	public String getUtilitzacio() {
		return utilitzacio;
	}
	public void setUtilitzacio(String utilitzacio) {
		this.utilitzacio = utilitzacio;
	}
	public String getConservacio() {
		return conservacio;
	}
	public void setConservacio(String conservacio) {
		this.conservacio = conservacio;
	}
	public Pesticida(EnumProductes tipus,String nomCom, String descripcio, double preuUnitat,
			double descompte, String referencia, int stock,
			EnumPlagues llistaEspecies, String utilitzacio, String conservacio) {
		super(tipus,nomCom, descripcio, preuUnitat, descompte, referencia, stock);
		this.llistaEspecies = llistaEspecies;
		this.utilitzacio = utilitzacio;
		this.conservacio = conservacio;
	}


	public String insert(){
		// TODO Auto-generated method stub
		String insert = super.insert() + getLlistaEspecies() + getUtilitzacio() + getConservacio();						
		return insert;
	}
	

	public String delete(){
		// TODO Auto-generated method stub
		String delete = super.insert() + getLlistaEspecies() + getUtilitzacio() + getConservacio();	
		return delete;
	}
	

	public String update(){
		// TODO Auto-generated method stub
		String update = super.insert() + getLlistaEspecies() + getUtilitzacio() + getConservacio();	
		return update();
	}
		
}
