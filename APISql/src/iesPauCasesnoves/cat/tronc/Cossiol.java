package iesPauCasesnoves.cat.tronc;

import iesPauCasesnoves.cat.enumeracions.EnumMaterial;
import iesPauCasesnoves.cat.enumeracions.EnumProductes;

public class Cossiol  extends Producte {
	private EnumMaterial material;
	private double capacitat;
	public EnumMaterial getMaterial() {
		return material;
	}
	public void setMaterial(EnumMaterial material) {
		this.material = material;
	}
	public double getCapacitat() {
		return capacitat;
	}
	public void setCapacitat(double capacitat) {
		this.capacitat = capacitat;
	}
	public Cossiol(EnumProductes tipus,String nomCom, String descripcio, double preuUnitat,
			double descompte, String referencia, int stock, EnumMaterial material,
			double capacitat) {
		super(tipus, nomCom, descripcio, preuUnitat, descompte, referencia, stock);
		this.material = material;
		this.capacitat = capacitat;
	}
	

	public String insert() {
		// TODO Auto-generated method stub
		String insert = super.insert() + getMaterial() + getCapacitat();						
		return insert;
	}
	

	public String delete(){
		// TODO Auto-generated method stub
		String delete = super.delete() + getMaterial() + getCapacitat();
		return delete;
	}
	

	public String update(){
		// TODO Auto-generated method stub
		String update = super.update() + getMaterial() + getCapacitat();
		return update;
	}
}
