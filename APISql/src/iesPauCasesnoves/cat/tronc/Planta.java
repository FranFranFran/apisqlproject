package iesPauCasesnoves.cat.tronc;

import iesPauCasesnoves.cat.enumeracions.EnumInteriorExterior;
import iesPauCasesnoves.cat.enumeracions.EnumProductes;

import java.util.ArrayList;

public class Planta extends Producte {
	private String nomCientific;
	private EnumInteriorExterior interiorExterior;
	private double frecuenciaReg;
	private double alzada;
	public String getNomCientific() {
		return nomCientific;
	}
	public void setNomCientific(String nomCientific) {
		this.nomCientific = nomCientific;
	}
	public EnumInteriorExterior getInterior_exterior() {
		return interiorExterior;
	}
	public void setInterior_exterior(EnumInteriorExterior interior_exterior) {
		this.interiorExterior = interior_exterior;
	}
	public double getFrecuenciaReg() {
		return frecuenciaReg;
	}
	public void setFrecuenciaReg(double frecuenciaReg) {
		this.frecuenciaReg = frecuenciaReg;
	}
	public double getAlzada() {
		return alzada;
	}
	public void setAlzada(double alzada) {
		this.alzada = alzada;
	}
	public Planta(EnumProductes tipus,String nomCom, String descripcio, double preuUnitat,
			double descompte, String referencia, int stock,
			String nomCientific, EnumInteriorExterior interior_exterior,
			double frecuenciaReg, double alzada) {
		super(tipus,nomCom, descripcio, preuUnitat, descompte, referencia, stock);
		this.nomCientific = nomCientific;
		this.interiorExterior = interior_exterior;
		this.frecuenciaReg = frecuenciaReg;
		this.alzada = alzada;
	}
	


	public String insert(){
		// TODO Auto-generated method stub
		String insert = super.insert() + getNomCientific() + getAlzada();
		return insert;
	}
	
	public String delete(){
		// TODO Auto-generated method stub
		String delete = super.delete() + getNomCientific() + getInterior_exterior() + getFrecuenciaReg() + getAlzada();	
		return delete;
	}
	
	
	public String update(){
		// TODO Auto-generated method stub
		String update = super.update() + getNomCientific() + getInterior_exterior() + getFrecuenciaReg() + getAlzada();	
		return update;
	}
	
	
	
	
}
