package iesPauCasesnoves.cat.tronc;

import iesPauCasesnoves.cat.enumeracions.EnumInteriorExterior;
import iesPauCasesnoves.cat.enumeracions.EnumMaterial;
import iesPauCasesnoves.cat.enumeracions.EnumPlagues;
import iesPauCasesnoves.cat.enumeracions.EnumProductes;

import java.util.ArrayList;

import cat.iespaucasesnoves.swpro.xml.generador.GeneradorFitxersXML;
import cat.iespaucasesnoves.swpro.xml.interficies.ConvertibleXML;

public class Magatzem implements ConvertibleXML {

	private String direccio;
	private int telefon;
	private String codi;

	private ArrayList<Producte> llistaProductes;
	private ArrayList<Cossiol> llistaCossiol;
	private ArrayList<Planta> llistaPlanta;
	private ArrayList<Pesticida> llistaPesticida;

	private static int nombreMagatzems = 1;

	public String getDireccio() {
		return direccio;
	}

	public void setDireccio(String direccio) {
		this.direccio = direccio;
	}

	public int getTelefon() {
		return telefon;
	}

	public void setTelefon(int telefon) {
		this.telefon = telefon;
	}

	public String getCodi() {
		return codi;
	}

	public void setCodi(String codi) {
		this.codi = codi;
	}

	public ArrayList<Producte> getLlistaProductes() {
		return llistaProductes;
	}

	public void setLlistaProductes(ArrayList<Producte> llistaProductes) {
		this.llistaProductes = llistaProductes;
	}

	public Magatzem(String direccio, int telefon, String codi,
			ArrayList<Producte> llistaProductes) {
		super();
		this.direccio = direccio;
		this.telefon = telefon;
		this.codi = codi;
		this.llistaProductes = llistaProductes;
	}

	/**
	 * Cerca per nom de producte Nom�s cal introduir part del nom comercial del
	 * producte per fer la cerca
	 * 
	 * @param nomCom
	 *            El par�metre a introduir �s del tipus String, partint d'aix�
	 *            es realitzar� l'operaci� de recerca
	 */
	public void cercaPartNom(String nomCom) {
		for (int i = 0; i < llistaProductes.size(); i++) {
			if (llistaProductes.get(i).getNomCom().contains(nomCom))
				System.out.println(llistaProductes.get(i));

		}
	}

	/**
	 * Cercador de planta per interior o exterior Aquest m�tode realitza una
	 * recerca de totes les plantes que hi ha a la base de dades i ens mostra
	 * totes les plantes que s�n d'interior o exterior, depenent del que
	 * introdu�m per par�metre.
	 * 
	 * @param interiorExterior
	 *            El par�metre a introduir ha de ser una constant de
	 *            l'enumeraci� "EnumInteriorExteriro". Les constants disponibles
	 *            s�n "INTERIOR" i "EXTERIOR". Acord amb aix� es realitzar� la
	 *            cerca.
	 * @return El m�tode ens retorna un ArrayList amb el resultat..
	 */
	public ArrayList cercaIntExtEnum(EnumInteriorExterior interiorExterior) {
		ArrayList listaprod = new ArrayList();
		for (Producte producte : llistaProductes) {
			if (producte.getTipus().equals(EnumProductes.PLANTA)) {
				Planta planta = (Planta) producte;
				if ((planta.getInterior_exterior().equals(interiorExterior))) {
					listaprod.add(planta);
				}
			}
		}
		return listaprod;
	}

	/**
	 * Cercador de cossiol por material Aquest m�tode realitza una recerca de
	 * tots els cossiols que hi ha a la base de dades i ens mostra tots els
	 * cossiols que s�n de pl�stic o de cer�mica, depenent del que introdu�m per
	 * par�metre.
	 * 
	 * 
	 * @param material
	 *            El par�metre a introduir ha de ser una constant de
	 *            l'enumeraci� "EnumMaterial". Les constants disponibles s�n
	 *            "PLASTIC" i "PORCELANA" Acord amb aix� es realitzar� la cerca.
	 * @return El m�tode ens retorna un ArrayList amb el resultat.
	 */

	public ArrayList cercarMaterial(EnumMaterial material) {
		ArrayList listaprod = new ArrayList();
		for (Producte producte : llistaProductes) {
			if (producte.getTipus().equals(EnumProductes.COSSIOL)) {
				Cossiol cossiol = (Cossiol) producte;
				if ((cossiol.getMaterial().equals(material))) {
					listaprod.add(cossiol);
				}
			}
		}
		return listaprod;
	}

	/**
	 * Cercador de pesticida per plagues Aques m�tode realitza una recerca de
	 * tots els pesticides que hi ha a la base de dades i ens mostra tots els
	 * pesticides siguin destinats a plagues de fongs, formigues o llimacs,
	 * depenent del que introdu�m per par�metre.
	 * 
	 * @param plagues
	 *            El par�metre a introduir ha de ser una constant de
	 *            l'enumeraci� "EnumPlagues". Les constants disponibles son
	 *            "FONGS", "FORMIGUES" i "LLIMACS". Acord amb aix� es realitzar�
	 *            la cerca.
	 * @return El m�tode ens retorna un ArrayList amb el resultat.
	 */
	public ArrayList cercarPesticida(EnumPlagues plagues) {
		ArrayList listaprod = new ArrayList();
		for (Producte producte : llistaProductes) {
			if (producte.getTipus().equals(EnumProductes.PESTICIDA)) {
				Pesticida pesticida = (Pesticida) producte;
				if ((pesticida.getLlistaEspecies().equals(plagues))) {
					listaprod.add(pesticida);
				}
			}
		}
		return listaprod;
	}

	/**
	 * Cercar productes per tipus Aquest m�tode realitza una recerca de tots el
	 * productes que hi ha a la base de dades i ens mostra tots els productes
	 * segons la seva categoria, ja siguin plantes, testos o pesticides,
	 * depenent del que introdu�m per par�metre.
	 * 
	 * @param tipus
	 *            El par�metre a introduir ha de ser una constant de l'
	 *            enumeraci� "EnumProductes". Les constants disponibles son
	 *            "COSSIOL", "PLANTA" i "PESTICIDA". Acord amb aix� es
	 *            realitzar� la cerca.
	 * @return El m�tode ens retorna un ArrayList amb el resultat.
	 */
	public ArrayList getProducte(EnumProductes tipus) {

		ArrayList listaprod = new ArrayList();

		for (Producte producte : llistaProductes) {
			if (producte.getTipus().equals(tipus)) {

				listaprod.add(producte);

			}
		}
		return listaprod;

	}

	/**
	 * M�todes per afegir Productes a la llista. no es necesari tenir un metode
	 * per inserir cossiol, un per incedir planta i un per inncedir pesticida.
	 * Amb insedir producte els tenim a tots en un de tot sol
	 */
	public void afegirProducte(Producte producte) {
		llistaProductes.add(producte);
	}

	@Override
	public String toString() {
		return "Magatzem [direccio=" + direccio + ", telefon=" + telefon
				+ ", codi=" + codi + ", llistaProductes=" + llistaProductes
				+ "]";
	}

	
	@Override
	public String getXML() {
		// TODO Auto-generated method stub
		String xml = "<magatzem>" + "<direccio>" + getDireccio()
				+ "</direccio>" + "<telefon>" + getTelefon() + "</telefon>"
				+ "<codi>" + getCodi() + "</codi>" + "</magatzem>";

		return xml;
	}
}
